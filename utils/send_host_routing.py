from convert import *
from socket import *
import sys
import time


VECTOR_SIZE = 6

if __name__ == "__main__":
    if len(sys.argv) < 4:
        print "Usage: python send_host_routing.py INTF MAC IP"
        sys.exit(1)

    payload = ""
    payload += encodeMac("ff:ff:ff:ff:ff:ff")
    payload += encodeMac(sys.argv[2])
    payload += encodeNum(0x553, 16)

    payload += encodeIPv4(sys.argv[3])  # src
    payload += encodeNum(1, 8)          # length
    payload += encodeIPv4(sys.argv[3])  # dst
    payload += encodeNum(0, 8)          # cost

    for i in range(1, VECTOR_SIZE):
        payload += encodeIPv4("0.0.0.0")
        payload += encodeNum(0, 8)

    s = socket(AF_PACKET, SOCK_RAW)

    # From the docs: "For raw packet sockets the address is a tuple:
    # (ifname, proto [,pkttype [,hatype]])"
    s.bind((sys.argv[1], 0))


    while 1:
        s.send(payload)
        time.sleep(10)